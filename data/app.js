function goToScanning()
{
    location.href = "/scanning";
}

function goToConfig()
{
    location.href = "/config";
}

function goToHome()
{
    location.href = "/";
}

function goToPowerOff()
{
    location.href = "/poweroff"
}

function goToDive()
{
    location.href = "/diveplan";
}

function goToBuddy()
{
    location.href= "/foundbuddy";
}

function goToReady()
{
    location.href = "/ready";
}